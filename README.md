# Proyecto simple en Django y MySql

Este proyecto fue creado siguiendo el curso https://www.udemy.com/curso-django-2-practico-desarrollo-web-python-3

## Prerrequisitos

Para ejecutarlo necesitas antes que nada tener previamente instalado en tu ordenador Python 3.x con Django 2.x y MySql corriendo

### Instalación

1 . Importar la base de datos que viene en el Git con la sigueinte configuración

```
'NAME': 'python',
'USER': 'root',
'PASSWORD': '',
'HOST': '127.0.0.1',
'PORT': '3306',
```

2 . Una vez importada la base de datos simplemente ponemos a correr el servidor Python

```
# Ir a la carpeta donde tenemos el proyecto y ejecutar lo siguiente:
python manage.py runserver
```

3 . Una vez el servidor este funcionando en su navegador debe abrir: http://127.0.0.1:8000/

4 . Fin!

## Autor

* **Jonathan Schell** - [The Coding Bear](http://thecodingbear.com)