from django.db import models

# Create your models here.


class Project(models.Model):
    title = models.CharField(max_length=200, verbose_name="Título")
    description = models.TextField(verbose_name="Descripción")
    image = models.ImageField(verbose_name="Imagen", upload_to="projects")
    url = models.URLField(verbose_name="Url", null=True, blank=True)
    # date of creation
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación")
    # date last update
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Decha de edición")

    class Meta:
        verbose_name = "proyecto"
        verbose_name_plural = "proyectos"
        ordering = ["-created"]
